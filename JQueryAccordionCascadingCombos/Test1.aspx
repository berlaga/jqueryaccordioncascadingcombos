﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test1.aspx.cs" Inherits="JQueryAccordionCascadingCombos.Test1" %>

<%@ Register Src="UserControls/JQueryAccordion.ascx" TagName="JQueryAccordion" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-right:50px; margin-top:50px;width:50%">

            <uc1:JQueryAccordion ID="JQueryAccordion1" runat="server" />

        </div>
    </form>
</body>
</html>
