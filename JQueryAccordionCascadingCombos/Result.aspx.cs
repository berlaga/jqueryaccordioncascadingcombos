﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JQueryAccordionCascadingCombos
{
    public partial class Result : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string result = Request.Form["JQueryAccordion1$ddlMake"] + " | " + Request.Form["JQueryAccordion1$ddlModel"] + " | " + Request.Form["JQueryAccordion1$ddlColor"];
            Label1.Text = result;
        }
    }
}