﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JQueryAccordion.ascx.cs" Inherits="JQueryAccordionCascadingCombos.UserControls.JQueryAccordion" %>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/start/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="../Scripts/jquery.cookie.js"></script>

<div>
    <label for ="ddlTheme">Select theme</label>
    <asp:DropDownList AutoPostBack="false" ID="ddlTheme" runat="server">
        <asp:ListItem Selected="True" Value="start" Text="Start"></asp:ListItem>
        <asp:ListItem Value="smoothness" Text="Smoothness"></asp:ListItem>
        <asp:ListItem Value="sunny" Text="Sunny"></asp:ListItem>
        <asp:ListItem Value="overcast" Text="Overcast"></asp:ListItem>
    </asp:DropDownList>
</div>
<p></p>
<div id="accordion">
    <h3>Step 1</h3>
    <div>
        <label for="ddlMake">Select make</label>
        <asp:DropDownList AutoPostBack="false" ID="ddlMake" runat="server">
            <asp:ListItem Value="-1" Text="Select.." Selected="True"></asp:ListItem>
            <asp:ListItem Value="1" Text="BMW"></asp:ListItem>
            <asp:ListItem Value="2" Text="Lexus"></asp:ListItem>
            <asp:ListItem Value="3" Text="Acura"></asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnApply1" runat="server" Text="Apply" OnClick="btnApply1_Click" />
    </div>
    <h3>Step 2</h3>
    <div>
        <div style="margin-bottom: 7px;">
            <label>You selected: </label>
            <asp:Label CssClass="bold" ID="lblMake" runat="server" Text="Label"></asp:Label>
        </div>
        <label for="ddlModel">Select model</label>
        <asp:DropDownList AutoPostBack="false" ID="ddlModel" AppendDataBoundItems="true" runat="server">
            <asp:ListItem Value="-1" Text="Select.." Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnApply2" runat="server" Text="Apply" OnClick="btnApply2_Click" />
    </div>
    <h3>Step 3</h3>
    <div>
        <div style="margin-bottom: 7px;">
            <label>You selected: </label>
            <asp:Label CssClass="bold" ID="lblModel" runat="server" Text="Label"></asp:Label>
        </div>
        <asp:DropDownList AutoPostBack="false" ID="ddlColor" AppendDataBoundItems="true" runat="server">
            <asp:ListItem Value="-1" Text="Select color" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="btnApply3" runat="server" Text="Apply" OnClick="btnApply3_Click" />
    </div>
    <h3>Step 4</h3>
    <div>
        <div style="margin: 7px; background-color: yellow;">
            <%= GetContentFillerText()%>
        </div>
        <div>
            <asp:Button ID="Button1" runat="server" Text="Submit" PostBackUrl="~/Result.aspx" />
        </div>
    </div>
</div>

<script>

  

    $(function () {
        $("#accordion").accordion();

        var theme = $.cookie("selected_theme");

        if (!theme) {
            $("link[rel=stylesheet]").attr({ href: "//code.jquery.com/ui/1.10.4/themes/" + $("#<%=ddlTheme.ClientID %>").val() + "/jquery-ui.css" });
            $.cookie("selected_theme", $("#<%=ddlTheme.ClientID %>").val());
        }
        else
            $("link[rel=stylesheet]").attr({ href: "//code.jquery.com/ui/1.10.4/themes/" + theme + "/jquery-ui.css" });


        if ("<%=(Page.IsPostBack).ToString().ToLower()%>" === "false") {
            $('#ui-accordion-accordion-header-1').unbind("click");
            $('#ui-accordion-accordion-header-2').unbind("click");
            $('#ui-accordion-accordion-header-3').unbind("click");
        }
        else {
            var index = $.cookie("selected_index");

            switch (index) {
                case "1":
                    $('#ui-accordion-accordion-header-2').unbind("click");
                    $('#ui-accordion-accordion-header-3').unbind("click");
                    $('#accordion').accordion({ active: 1 });
                    break;
                case "2":
                    $('#ui-accordion-accordion-header-3').unbind("click");
                    $('#accordion').accordion({ active: 2 });
                    break;
                case "3":
                    $('#ui-accordion-accordion-header-3').unbind("click");
                    $('#accordion').accordion({ active: 3 });
                    break;
            }
        }

        $("#<%=btnApply1.ClientID%>").click(function (event) {

            if ($("#<%=ddlMake.ClientID %>>option:selected").val() == "-1") {
                alert("Select make");
                event.preventDefault();
            }
            else {
                $('#ui-accordion-accordion-header-1').bind("click");
                $('#ui-accordion-accordion-header-1').click();
                $.cookie("selected_index", 1);
            }

        });

        $("#<%=btnApply2.ClientID%>").click(function (event) {

            if ($("#<%=ddlModel.ClientID %>>option:selected").val() == "-1") {
                alert("Select model");
                event.preventDefault();
            }
            else {
                $('#ui-accordion-accordion-header-2').bind("click");
                $('#ui-accordion-accordion-header-2').click();
                $.cookie("selected_index", 2);
            }

        });

        $("#<%=btnApply3.ClientID%>").click(function (event) {

            if ($("#<%=ddlColor.ClientID %>>option:selected").val() == "-1") {
                alert("Select color");
                event.preventDefault();
            }
            else {
                $('#ui-accordion-accordion-header-3').bind("click");
                $('#ui-accordion-accordion-header-3').click();
                $.cookie("selected_index", 3);
            }

        });

        $("#accordion").on("accordionbeforeactivate", function (event, ui) {

            var index = $("#accordion").accordion("option", "active");

        });


        $("#<%=ddlTheme.ClientID %>").change(function () {
            $("link[rel=stylesheet]").attr({ href: "//code.jquery.com/ui/1.10.4/themes/" + this.value + "/jquery-ui.css" });
            $.cookie("selected_theme", this.value);
        });
    });
</script>
