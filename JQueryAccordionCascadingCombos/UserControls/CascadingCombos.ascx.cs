﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JQueryAccordionCascadingCombos.UserControls
{
    public partial class CascadingCombos : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetContentFillerText()
        {
            string result = string.Format("You selected: {0} | {1} | {2}", ddlMake.SelectedItem.Text, ddlModel.SelectedItem.Text, ddlColor.SelectedItem.Text);
            return result;
        }

        protected void btnApply1_Click(object sender, EventArgs e)
        {
            lblMake.Text = ddlMake.SelectedItem.Text;

            ListItem li = new ListItem();
            ddlModel.Items.Clear();

            switch(ddlMake.SelectedItem.Value)
            {
                case "-1":
                    break;
                case "1":
                    li = new ListItem("X1", "1");
                    ddlModel.Items.Add(li);
                    li = new ListItem("X5", "2");
                    ddlModel.Items.Add(li);
                    li = new ListItem("X6", "3");
                    ddlModel.Items.Add(li);
                    break;
                case "2":
                    li = new ListItem("RX350", "1");
                    ddlModel.Items.Add(li);
                    li = new ListItem("RX470", "2");
                    ddlModel.Items.Add(li);
                    break;
                case "3":
                    li = new ListItem("RDX", "1");
                    ddlModel.Items.Add(li);
                    li = new ListItem("MDX", "2");
                    ddlModel.Items.Add(li);

                    break;
            }
        }

        protected void btnApply2_Click(object sender, EventArgs e)
        {
            lblModel.Text = ddlModel.SelectedItem.Text;

            ddlColor.Items.Clear();

            ListItem li = new ListItem("Red","1");
            ddlColor.Items.Add(li);
            li = new ListItem("Blue", "2");
            ddlColor.Items.Add(li);
            li = new ListItem("Black", "3");
            ddlColor.Items.Add(li);

        }

        protected void btnApply3_Click(object sender, EventArgs e)
        {

        }

     
    }
}