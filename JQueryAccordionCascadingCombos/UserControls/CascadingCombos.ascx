﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CascadingCombos.ascx.cs" Inherits="JQueryAccordionCascadingCombos.UserControls.CascadingCombos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<script src="../Scripts/jquery-1.8.2.js"></script>
<script type="text/javascript">

	$(document).ready(function () {
		var accordion = $get("<%=MyAccordion.ClientID%>").AccordionBehavior;

		$("#<%=btnApply1.ClientID%>").click(function (event) {

			if ($("#<%=ddlMake.ClientID %>>option:selected").val() == "-1") {
				alert("Select make");
				event.preventDefault();
			}
			else
				accordion.set_SelectedIndex(1);
		});
		$("#<%=ddlMake.ClientID%>").change(function () {
			for (var i = 1; i < 4; i++) {
				var header = accordion.get_Pane(i).header;
				$removeHandler(header, "click", accordion._headerClickHandler);
			}
		});


		$("#<%=btnApply2.ClientID%>").click(function () {
			accordion.set_SelectedIndex(2);
		});
		$("#<%=ddlModel.ClientID%>").change(function () {
			for (var i = 2; i < 4; i++) {
				var header = accordion.get_Pane(i).header;
				$removeHandler(header, "click", accordion._headerClickHandler);
			}
		});


		$("#<%=btnApply3.ClientID%>").click(function () {
			accordion.set_SelectedIndex(3);
		});


		if ("<%=(Page.IsPostBack).ToString().ToLower()%>" === "false") {
			for (var i = 1; i < 4; i++) {
				var header = accordion.get_Pane(i).header;
				$removeHandler(header, "click", accordion._headerClickHandler);
			}
		}
		else {
			var index = accordion.get_SelectedIndex();

			switch (index) {
				case 1:
					for (var i = 2; i < 4; i++) {
						var header = accordion.get_Pane(i).header;
						$removeHandler(header, "click", accordion._headerClickHandler);
					}
					break;
				case 2:
					for (var i = 3; i < 4; i++) {
						var header = accordion.get_Pane(i).header;
						$removeHandler(header, "click", accordion._headerClickHandler);
					}
					break;
				case 3:
					var header = accordion.get_Pane(3).header;
					$removeHandler(header, "click", accordion._headerClickHandler);
					break;
			}
		}

		


		//accordion.add_selectedIndexChanged(
		//        function (sender, args) {
		//            var newIndex = args.get_selectedIndex();
		//            if (newIndex === 1) {

		//                var header = accordion.get_Pane(1).header;
		//                $removeHandler(header, "click", accordion._headerClickHandler);
		//            }
		//        }
		//    );

	});

</script>

<style type="text/css">
	.bold {
		font-weight: bolder;
	}
</style>

<%--ScriptMode="Release" to fix javascript error
http://forums.asp.net/t/1591882.aspx?+Handler+was+not+added+through+the+Sys+UI+DomEvent+addHandler+method+error+with+checkbox+and+MutuallyExclusiveCheckBoxExtender
	--%>
<ajaxToolkit:ToolkitScriptManager ScriptMode="Release" runat="server" ID="ScriptManager1">
	<ControlBundles>
		<ajaxToolkit:ControlBundle Name="Accordion" />
	</ControlBundles>
</ajaxToolkit:ToolkitScriptManager>

<ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
	HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
	ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40"
	TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
	<Panes>
		<ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
			<Header><a href="#" class="accordionLink">Step 1</a></Header>
			<Content>
				<label for="ddlMake">Select make</label>
				<asp:DropDownList AutoPostBack="false" ID="ddlMake" runat="server">
					<asp:ListItem Value="-1" Text="Select.." Selected="True"></asp:ListItem>
					<asp:ListItem Value="1" Text="BMW"></asp:ListItem>
					<asp:ListItem Value="2" Text="Lexus"></asp:ListItem>
					<asp:ListItem Value="3" Text="Acura"></asp:ListItem>
				</asp:DropDownList>
				<asp:Button ID="btnApply1" runat="server" Text="Apply" OnClick="btnApply1_Click" />
			</Content>
		</ajaxToolkit:AccordionPane>
		<ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
			<Header><a href="#" class="accordionLink">Step 2</a></Header>
			<Content>
				<div style="margin-bottom: 7px;">
					<label>You selected: </label>
					<asp:Label CssClass="bold" ID="lblMake" runat="server" Text="Label"></asp:Label>
				</div>
				<label for="ddlModel">Select model</label>
				<asp:DropDownList ID="ddlModel" AppendDataBoundItems="true" runat="server">
					<asp:ListItem Value="-1" Text="Select.." Selected="True"></asp:ListItem>
				</asp:DropDownList>
				<asp:Button ID="btnApply2" runat="server" Text="Apply" OnClick="btnApply2_Click" />
			</Content>
		</ajaxToolkit:AccordionPane>
		<ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
			<Header><a href="#" class="accordionLink">Step 3</a></Header>
			<Content>
				<div style="margin-bottom: 7px;">
					<label>You selected: </label>
					<asp:Label CssClass="bold" ID="lblModel" runat="server" Text="Label"></asp:Label>
				</div>
				<asp:DropDownList ID="ddlColor" AppendDataBoundItems="true" runat="server">
					<asp:ListItem Value="-1" Text="Select color" Selected="True"></asp:ListItem>
				</asp:DropDownList>
				<asp:Button ID="btnApply3" runat="server" Text="Apply" OnClick="btnApply3_Click" />
			</Content>
		</ajaxToolkit:AccordionPane>
		<ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
			<Header><a href="#" class="accordionLink">Step 4</a></Header>
			<Content>
				<div style="margin: 7px; background-color:yellow;">
					<%= GetContentFillerText()%>
				</div>
			</Content>
		</ajaxToolkit:AccordionPane>
	</Panes>
</ajaxToolkit:Accordion>



