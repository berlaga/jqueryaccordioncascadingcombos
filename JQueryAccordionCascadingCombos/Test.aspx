﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="JQueryAccordionCascadingCombos.Test" %>

<%@ Register Src="UserControls/CascadingCombos.ascx" TagName="CascadingCombos" TagPrefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Combo test</title>
<%--    <script src="Scripts/jquery-1.8.2.js"></script>
    <script src="Scripts/jquery-ui-1.8.24.js"></script>--%>
    <link href="Styles/StyleSheet.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 50%;">

            <uc1:CascadingCombos ID="CascadingCombos1" runat="server" />

        </div>
    </form>
</body>
</html>
